#include <stdio.h>
#include <string.h>

struct auto
{
  int ano;
  float kilometraje;
  char marca[10];
  char modelo[10];
  char estado[10];
  char color[6];
};

struct computadora
{
  int memoria;
  int año;
  char marca[15];
  char color[15];
  char generacion[10];
};

struct alumno
{
  int edad;
  float estatura;
  char nombre[25];
  char nCuenta[15];
  char carrea[20];
  char Facultad[25];
};

struct equipoDeFutbol
{
  int cantidad de jugadores;
  char color de uniforme[15];
  int nJUgadores;
  int posicion;
};

struct Mascota
{
  int edad;
  float peso;
  char nombre[15];
  char raza[15];
  char color[10];
};

struct VideoJuego
{
  int costo;
  char Titulo[15];
  char Categoria[15];
  char Consola[10];
};

struct SuperHeroe
{
  int edad;
  float estatura;
  char Nombre[15];
  char Poderes[150];
  char Equipo[15];
  char Traje[20];
};

struct Planetas
{
  int antiguedad;
  float dimensiones;
  int lunas;
  char nombre[15];
  char galaxia[15];
};

struct CristianoRonaldo
{
  int edad;
  float estatura;
  int goles[35];
  char nombre[15];
  char equipo[20];
  char seleccion[25];
  int campeonatos;

}
